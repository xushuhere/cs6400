//verify the user login 
//assume the application handles the login verification
//read $Email, $Password from the input
SELECT *
FROM USER
WHERE Email=$Email AND Password=$Password;

//Create a Profile
//read $Email, $Password, $Password_conf, $FirstName, $LastName
INSERT INTO USER
VALUES (Email=$Email, Password=$Password, FirstName=$FirstName, LastName=$LastName);

INSERT INTO CUSTOMERS
VALUES (Email=$Email,CountryCodeH=$CountryCodeH, LocalNumH= $LocalNumH, CountryCodeW=$CountryCodeW, LocalNumW =$LocalNumW, Address=$Address);

//view Customer Profile, $Email is handled over the database
SELECT Cu.Email, Cu.FirstName, Cu.LastName,Cu.CountryCodeH, Cu.LocalCodeH, Cu.CountryCodeW, Cu.LocalCodeW, Cu.Address
FROM CUSTOMERS as Cu 	
WHERE Cu.Email = $CuEmail AND R.CuEmail= $Email
//Reservation History
SELECT R.ResID,T.ToolID, R.ResStartDate, R.ResEndDate, R.RentalPrice, R.Deposit, CLP.FirstName, CLP.FirstName
FROM RESERVATIONS as R 
	JOIN CONTAIN as CON ON R.ResID=CON.RID
	JOIN TOOLS as T on T.TOOLID = Con.TID
	JOIN CLERKS as CLP on CL.Email = R.ClEmailPickup
	JOIN CLERKS as CLD on CL.Email = R.ClEmailDropoff
WHERE R.CuEmail = $Email
ORDER BY 
   CONVERT(DateTime, R.ResStartDate,101)  DESC;

//Make Reservation
//assume $Email of current user is managed by application, ResID is assigned as TEMPID before submission, $ResStartDate, $ResEndDate are handled through the application
INSERT INTO RESERVATIONS
(ResID, CuEMail, ClEmailDropOFF, ClEmailPickup, ResStartDate, ResEndDate, CreditCardNo, ExpDate) 
VALUES (“TEMPID”, $Email, NULL, NULL, $ResStartDate, $ResEndDate, NULL, NULL)

//Add a tool into the reservation, $ToolID and $ResID are handled via the application
IF (SELECT COUNT(ToolID) FROM CONTAIN < 50)
INSERT INTO CONTAIN
(ResID, ToolID)
VALUES(“TEMPID”, $ToolID);

//when $tooltype is selected, displaying the dropdown for this type of Tools
SELECT T.ToolID, T.AbbrDes, T.RentPrice, H.ToolID, R. ResStartDate, R. ResEndDate
FROM TOOLS AS T, $Tooltype AS H, RESERVATIONS AS R 
WHERE T.ToolID = H.ToolID AND T.ToolID NOT IN (
SELECT C.TID
FROM RESERVATIONS AS R, CONTAIN AS C
WHERE CURDATE()<=R.ResEndDate AND CURDATE()>=R.ResStartDate AND R.ResID=C.RID);

//When tool is selected
INSERT INTO CONTAIN
(ResID, ToolID)
VALUES(“TEMPID”, $ToolID);

//If Remove Last Tool: Remove the last drop down lists for Type of Tool and Tool
Delete LastTools
DELETE LAST(ToolID) FROM CONTAIN
WHERE CONTAIN.RID=“TEMPID”;

//Calculate Total: Calculate the Total Rental Price and Total Deposit Required;
Display Tools Desired, Start Date, End Date, Total Rental Price, and Total Deposit
Required;
SELECT C.ResID, C.ToolID, R.ResID, R.ResStartDate, R.ResEndDate, T.ToolID,  T.RentPrice, T.RentDeposit
FROM CONTAIN AS C, RESERVATIONS AS R,TOOLS AS T 
WHERE C.RID = R.ResID AND T.ToolID = C.TID AND C.RID=“TEMPID”

//If Submit: Display Reservation Number(generate unique $ResID by application), Tools Desired, Start Date, End Date, Total
Rental Price, and Total Deposit Required;
UPDATE CONTAIN
SET CONTAIN.RID = $ResID
WHERE CONTAIN.RID = "TEMPID";

SELECT C.RID, C.TID, R.ResID, R.ResStartDate, R.ResEndDate, T.ToolID,  T.RentPrice, T.Deposit
FROM CONTAIN AS C, RESERVATIONS AS R,TOOLS AS T
WHERE C.RID = R.ResID AND T.ToolID = C.TID AND C.RID= $ResID

//If Reset: clear everything; go back to the Make Reservation screen;}
DELETE * FROM CONTAIN, RESERVATIONS
WHERE CONTAIN.RID=“TEMPID”;


//Pickup
SELECT R.ResID, T.ToolID, T.AbbrDes, T.Deposit, T.RentPrice*DATEDIFF(day,R.ResStartDate,R.ResEndDate) AS Estimated_cost
FROM RESERVATIONS AS R, TOOLS AS T, CONTAIN AS C
WHERE R.ResID=$ResID AND C.RID=R.ResID AND C.TID=T.ToolID;

UPDATE RESERVATIONS
SET ClEmailPickUp=$Email, CreditCardNo = $CreditCardNo, ExpDate = $ExpirationDate
WHERE ResID=$ResID;

//Pickup view detail tool
SELECT *
FROM TOOLS
WHERE ToolID = $ToolID;

//dropoff
UPDATE RESERVATIONS 
SET ClEmailDropOff=$Email
WHERE ResID=$ResID;

SELECT R.ResID, UCl.FirstName, UCl.LastName, UCu.FirstName, UCu.LastName, R.CuEmail, R.ResStartDate, R.ResEndDate, T.RentPrice*DATEDIFF(day,R.ResStartDate,R.ResEndDate) AS RentalPrice, T.Deposit, R.CreditCardNo, T.RentPrice* DATEDIFF (day,R.ResStartDate,R.ResEndDate)-T.Deposit AS Total
FROM RESERVATIONS AS R, USER AS UCl, USER AS UCu, TOOLS AS T, CONTAIN AS C
WHERE R.ResID=$ResID AND UCl.Email=R.ClEmailDropoff AND UCu.Email=R.CuEmail AND C.RID=R.ResID AND C.TID=T.ToolID;
 

//check availability of tools
SELECT ToolID, AbbrDes, RentPrice, Deposit
FROM TOOLS
WHERE ToolID NOT IN (
SELECT C.TID
FROM RESERVATIONS AS R, CONTAIN AS C
WHERE CURDATE()<=R.ResEndDate AND CURDATE()>=R.ResStartDate AND R.ResID=C.RID);


//Add new tool
INSERT INTO TOOLS (ToolID,PurchasePrice,FullDes,RentPrice,Deposit,ClEmailAddTool)
VALUES ($ToolID, $PurchasePrice, $AbbrDes, $FullDes, $RentPrice, $Deposit, $ClEmailAddTool);

INSERT INTO $ToolCatalog (ToolID)
VALUES ($ToolID);

//If add a powertool
INSERT INTO POWERTOOLS_ACCES
VALUES ($ToolID, $Accessories);

//Sell tools
SELECT ToolID, PurchasePrice/2 AS SellPrice
FROM TOOLS
WHERE ToolID=$ToolID;

UPDATE TOOLS
SET ClEmailSale=$ClEmail
WHERE ToolID=$ToolID;


//Service order
INSERT INTO SERVICE_REQUEST
VALUES ($TID, $SDate, $ClEmailService, $Endate, $EstCost);


//generate report
//First report
SELECT T.ToolID, T.AbbrDes, SUM(DATEDIFF (day,R.ResStartDate,R.ResEndDate)*T.RentPrice) AS RentalProfit, T.PurchasePrice+SUM(S.EstCost) AS ToolCost, SUM(DATEDIFF (day,R.ResStartDate,R.ResEndDate)*T.RentPrice)-(T.PurchasePrice+SUM(S.EstCost)) AS TotalProfit 
FROM TOOLS AS T, RESERVATIONS AS R, REPAIR_SERVICE_REQUEST AS S
WHERE T.ToolID=S.TID
GROUP BY T.ToolID
ORDER BY TotalProfit;

//Second report
SELECT U.FirstName, U.LastName, C.Email, COUNT(R.CuEMail) AS nRentals
FROM USER AS U, CUSTOMERS AS C, RESERVATIONS AS R
WHERE C.Email=R.CuEMail AND U.Email=C.Email
GROUP BY R.CuEMail
ORDER BY nRentals, U.LastName;

//Final report

CREATE VIEW temp1 AS (SELECT U.FirstName, U.LastName, COUNT(RP.ClEmailPickUp) AS nPickup
FROM USER AS U, RESERVATIONS AS RP
WHERE U.Email=RP.ClEmailPickUp AND RP.ResStartDate=MONTH(getdate())
GROUP BY RP.ClEmailPickUp);

CREATE VIEW temp2 AS (SELECT U.FirstName, U.LastName, COUNT(RD.ClEmailDropOff) AS nDropoff
FROM USER AS U, RESERVATIONS AS RD
WHERE U.Email=RD.ClEmailDropOff AND RD.ResStartDate=MONTH(getdate())
GROUP BY RD.ClEmailDropOff);

SELECT T1.FirstName, T1.LastName, T1.nPickup, T2.nDropoff, T1.nPickup+T2.nDropoff AS nTotal
FROM temp1 AS T1 
INNER JOIN temp2 as T2 ON (T1.FirstName=T2.FirstName)
INNER JOIN temp2 as T3 ON (T1.LastName=T3.LastName)
ORDER BY nTotal;

//After report generated, delete temporary views
DROP VIEW temp1,temp2;




